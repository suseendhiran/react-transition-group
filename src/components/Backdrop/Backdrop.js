import React from "react";

import "./Backdrop.css";

const backdrop = (props) => {
  const BackdropClass = [
    "Backdrop",
    props.show ? "BackdropOpen" : "BackdropClose",
  ];

  return <div className={BackdropClass.join(" ")}></div>;
};

export default backdrop;
