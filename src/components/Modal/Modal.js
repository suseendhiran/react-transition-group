import React, { useEffect } from "react";
import CSSTransition from "react-transition-group/CSSTransition";

import "./Modal.css";

function modal(props) {
  console.log(props);

  // const ModalClass = [
  //   "Modal",
  //   props.state == "entering"
  //     ? "ModalOpen"
  //     : props.state == "exiting"
  //     ? "ModalClose"
  //     : null,
  // ];

  return (
    <CSSTransition
      in={props.show}
      timeout={{ enter: 400, exit: 600 }}
      mountOnEnter
      unmountOnExit
      classNames={{
        enterActive: "ModalOpen",
        exitActive: "ModalClose",
      }}
    >
      <div className="Modal">
        <h1>A Modal</h1>
        <button className="Button" onClick={props.closed}>
          Dismiss
        </button>
      </div>
    </CSSTransition>
  );
}

export default modal;
