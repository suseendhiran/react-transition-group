import React, { useEffect } from "react";
import Transition from "react-transition-group/Transition";

import "./Modal.css";

function modal(props) {
  console.log(props);

  // const ModalClass = [
  //   "Modal",
  //   props.state == "entering"
  //     ? "ModalOpen"
  //     : props.state == "exiting"
  //     ? "ModalClose"
  //     : null,
  // ];

  return (
    <Transition
      in={props.show}
      timeout={{ enter: 400, exit: 600 }}
      mountOnEnter
      unmountOnExit
    >
      {(state) => {
        const ModalClass = [
          "Modal",
          state == "entering"
            ? "ModalOpen"
            : state == "exiting"
            ? "ModalClose"
            : null,
        ];
        return (
          <div className={ModalClass.join(" ")}>
            <h1>A Modal</h1>
            <button className="Button" onClick={props.closed}>
              Dismiss
            </button>
          </div>
        );
      }}
    </Transition>
  );
}

export default modal;
