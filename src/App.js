import React, { Component } from "react";
import Transition from "react-transition-group/Transition";
import "./App.css";
import Modal from "./components/Modal/Modal";
import Backdrop from "./components/Backdrop/Backdrop";
import List from "./components/List/List";

class App extends Component {
  state = {
    modalOpen: false,
    toggleShow: false,
  };

  openModal = () => {
    console.log("kll");
    this.setState({ modalOpen: true });
  };

  closeModal = () => {
    console.log("kll");
    this.setState({ modalOpen: false });
  };

  render() {
    return (
      <div className="App">
        <h1>React Animations</h1>

        {this.state.modalOpen ? <Backdrop show={this.state.modalOpen} /> : null}

        <br />

        <Modal closed={this.closeModal} show={this.state.modalOpen} />

        {/* {this.state.toggleShow ? (
          <div
            style={{
              background: "red",
              width: 100,
              height: 100,
              margin: "auto",
            }}
          />
        ) : null} */}
        <button
          className="Button"
          onClick={() => {
            this.openModal();
          }}
        >
          Open Modal
        </button>
        <h3>Animating Lists</h3>
        <List />
      </div>
    );
  }
}

export default App;
